import React from "react";

export default function Root(props) {
  return (
    <section>
      HOLO App Core --
      {props.name} is mounted!
    </section>
  );
}
